# DNS System

## What is DNS ?
- Domain Name Service (DNS): phân giải tên miền thành địa chỉ IP và ngược lại
- DNS sử dụng UDP/53 và TCP/53
  + UDP/53: port dùng để DNS giao tiếp với client để phân giải tên miền
  + TCP/53: port dùng để transfer file zone giữa các DNS Server
- Các record cơ bản của DNS Server
  + Record A : ánh xạ tên miền thành địa chỉ IPv4
  + Record AAAA : ánh xạ tên miền thành địa chỉ IPv6
  + Record PTR : ánh xạ một địa chỉ IP thành tên máy chủ
  + Record TXT : admin stores text note in the record
  + Record NS : xác định Name Server trong domain
  + Record MX : xác định Mail Server trong domain
  + Record CNAME : định nghĩa "bí danh" ứng với tên miền
  + Record SOA (Start of Authority) : chứa thông tin zone admin
## How does DNS work ?
![Alt text](images/image0.png)
1. người dùng nhập ‘www.example.com’ vào thanh tìm kiếm vào nhấn enter → browser gửi domain đến trình phân giải DNS (DNS resolver)
2. DNS resolver cho ISP gửi yêu cầu DNS query đến DNS root name server
3. DNS root name server sẽ phản hồi về cho DNS resolver với địa chỉ của TLP DNS server (.com)
4. sau đó resolver tạo một request đến .com TLD server
5. sau đó TLD server sẽ phản hồi với địa chỉ IP của domain name server (example.com)
6. DNS resolver gửi query đến authoritative name server
7. authoritative name server trả về địa chỉ IP của example.com (93.184.216..34) đến DNS resolver
## DNS Server Master-Slave
### 1. Master-Slave
- Master server :
  + là nơi thay đổi data DNS zones
  + khi update zone ⇒ tăng số serial trong SOA record ⇒ thông báo cho DNS slave có bản cập nhật mới
- Slave server :
  + check định kỳ DNS master để cập nhật bằng cách gửi NOTIFY message
  + nếu serial (master) > serial (slave) ⇒ slave update data zones từ master
### 2. DNS Lookup work
- user requests to web or app thì DNS query đã gửi đến master
- master phân giải domain name để tương ứng với địa chỉ IP, nhưng nếu master down thì request được chuyển sang cho slave
- slave phân giải domain name và trả về địa chỉ IP cho thiết bị đang request và cho phép được truy cập vào web or app
### 3. How does the DNS master transmit data to the DNS Slave?
![Alt text](images/image-1.png)

=> zone transfer : sao chép toàn bộ nội dung của DNS zone từ master sang slave
- DNS Primary luôn load hoặc reload file zone từ nơi lưu trữ file local hoặc được nối mạng

- DNS secondary luôn load zone data từ primary thông qua zone trasfer ( sao chép toàn bộ nội dung của DNS zone từ master sang slave ), zone transfer có thể sử dụng Full zone transfer ( AXFR ) hoặc Incremental Zone Transfer ( IXFR ) 
  + Full zone transfer ( AXFR )
    + Khởi tạo yêu cầu (slave) : slave gửi query ( chứa domain name của zone được yêu cầu và kiểu yêu cầu (AXFR) ) đến master
    + Master response : master nhận request AXFR và phản hồi bằng cách cung cấp toàn bộ nội dung của zone đến slave
    + Data transmission : master truyền đi DNS record cho toàn bộ zone ( tất cả tài nguyên records ( SOA, A, AAAA, CNAME, MX, NS, TXT ) và metadata (TTL, cài đặt cấu hình riêng của zone) đến slave
    + Slave update : slave nhận và lưu trữ zone data đã được master chuyển sang ⇒ update file local zone để match với data đã được chuyển qua
  + Incremental Zone Transfer ( IXFR )
  => hiệu quả hơn full zone transfer : chỉ chuyển đổi thay đổi mới nhất của zone ⇒ giảm số lượng dữ liệu được chuyển đổi
    + Khởi tạo yêu cầu (slave) : slave gửi query ( chứa domain name của zone được yêu cầu và kiểu yêu cầu (IXFR) ) đến master
    + Master Response : master check số serial trong SOA record ⇒ xác định có thực hiện chuyển đổi hay không , nếu số serial thay đổi (từ lần truyền mới nhất) ⇒ master phản hồi về 1 bộ thay đổi record hoặc thông báo cần phải chuyển toàn bộ
    + Data Transmission : master gửi 1 bộ record thay đổi (additions, deletions modifications) tử bản chuyển đổi mới nhất ⇒ slave update từ dữ liệu local zone dựa trên những gì đã thay đổi
    + Fallback to Full Transfer : nếu master xác định truyền ko khả thi do thiếu số serial hoặc do lí do khác ⇒ master hướng dẫn slave thực hiện quá trình truyền đầy đủ (AXFR)
### 4. Configure DNS Server Master-Slave
##### a. Mô hình DNS Server Master - Slave
- 1 máy DNS Server Master ( DNS authoritative) : 192.168.188.131
- 1 máy DNS Server Slave 1 ( DNS authoritative) : 192.168.188.132
- 1 máy DNS Server Slave 2 ( DNS authoritative) : 192.168.188.134
- 1 máy DNS Client ( DNS resolver) : 192.168.188.130
##### b. Setup DNS server Master
- Cài đặt chương trình bind9
  ```
    $ sudo apt update
    $ sudo apt install bind9 bind9utils bind9-doc
  ```
- Start bind9 DNS server
  ```
    $ sudo service bind9 start 
    $ sudo service bind9 status
  ```
- Cấu hình các thông số cho bind9 DNS Server
  + Cấu hình file "/etc/default/named"
    ```
      $ sudo vim /etc/default/named
    ```
    ```bash
      OPTIONS="-u bind -4"
    ```
  + Cấu hình file "/etc/named.conf.options"
  ```
    $ sudo vim /etc/bind/named.conf.options
  ```
  ```bash
    acl "trusted" {
        192.168.188.131;    # ns1 
        192.168.188.132;   # ns2
        192.168.188.130;  # host1
    };

    logging {
      channel bind_log {
        file "bind.log" versions 3 size 250k;
        severity info;
      };
      category default {
        bind_log;
      };
    };

    options {
      directory "/var/cache/bind";

      dnssec-validation auto; #auto-verification integrated with DNSSEC
      recursion yes;  # enables recursive queries
      allow-recursion { trusted; }; # allows recursive queries from "trusted" clients
      listen-on { 192.168.188.131; }; # ns1 private IP address - listen on private network only
      allow-transfer { none; }; # disable zone transfers by default
      forwarders {
              8.8.8.8;
              8.8.4.4;
      };
    };

  ```
  + Cấu hình file "/etc/named.conf.local"
  ```
    $ sudo vim /etc/bind/named.conf.local
  ```
  ```bash
    # Định nghĩa Forward zone "vmware.lab" 
    zone "vmware.lab" {
            type primary;
            file "/etc/bind/zones/db.vmware.lab"; #zone file path
            allow-update { key rndc-key; };
            allow-transfer { 192.168.188.132; 192.168.188.133 };  # ns2 private IP address secondary
            also-notify { 192.168.188.132; 192.168.188.133 } ;      # notify slave for zone changes
            notify yes;
    };

    # Định nghĩa Reverse zone "188.168.192.in-addr.arpa" ứng với Forward zone
    zone "188.168.192.in-addr.arpa" {
            type primary;
            file "/etc/bind/zones/db.192.168.188"; #192.168.188.0/24 subnet
            allow-update { key rndc-key; };
            allow-transfer { 192.168.188.132; 192.168.188.133 };  # ns2 private IP address secondary
            also-notify { 192.168.188.132; 192.168.188.133 } ;     # notify slave for zone changes
            notify yes;
    };

  ```
  + Cấu hình forward zone "/etc/bind/zones/db.vmware.lab"
  ```
    $ sudo mkdir /etc/bind/zones
    $ sudo vim /etc/bind/zones/db.vmware.lab
  ```
  ```bash
    $TTL    604800

    @   IN    SOA   ns1.vmware.lab.   admin.vmware.lab. (

                              3         ; Serial

                         604800         ; Refresh

                          86400         ; Retry

                        2419200         ; Expire

                         604800 )       ; Negative Cache TTL

    ;

    ; name servers - NS records

    @     IN      NS      ns1.vmware.lab.

    @     IN      NS      ns2.vmware.lab.

    @     IN      NS      ns3.vmware.lab.

    ; name servers - A records

    ns1.vmware.lab.          IN      A       192.168.188.131

    ns2.vmware.lab.          IN      A       192.168.188.132

    ns3.vmware.lab.          IN      A       192.168.188.133

    ; 192.168.188.0/24- A records

    vn.vmware.lab.           IN      A      192.168.188.130
  ```
  + Cấu hình reverse zone "/etc/bind/zones/db.192.168.188"
  ```
    $ sudo vim /etc/bind/zones/db.192.168.188
  ```
  ```bash
    $TTL    604800

    @       IN      SOA     vmware.lab. admin.vmware.lab. (

                                 3         ; Serial

                            604800         ; Refresh

                             86400          ; Retry

                            2419200         ; Expire

                            604800 )       ; Negative Cache TTL

    ; name servers

    @      IN      NS      ns1.vmware.lab.

    @      IN      NS      ns2.vmware.lab.

    @      IN      NS      ns3.vmware.lab.

    ; PTR Records

    131   IN      PTR     ns1.vmware.lab.    ; 192.168.188.131

    132   IN      PTR     ns2.vmware.lab.    ; 192.168.188.132

    133   IN      PTR     ns3.vmware.lab.    ; 192.168.188.133

    130  IN      PTR     vn.vmware.lab.  ; 192.168.188.130
  ```
  + Giải thích các thông số :
    - ***TTL 604800*** : time to live (tgian sống mặc định - 7 ngày) - xác định thời gian tối đa dữ liệu DNS được lưu trữ trong cache của DNS server hoặc máy tính client trước khi được truy vấn lại DNS server master
    - ***@*** : đại diện cho tên miền gốc của zone
    - ****.<domain_name>*** : * đại diện cho tất các các subdomain của domain name đó
    - ***IN*** : internet
    - ***NS*** : xác định DNS Server ở trong domain
    - ***PTR*** : phân giải IP sang tên miền
    - ***SOA*** : start of authority - bản ghi đầu cho domain DNS - chứa thông tin quản trị về tiên miền, xác định các thông số quan trọng cho việc quản lý và cập nhật dữ liệu DNS
      + client : xác định time-to-live tối đa lưu trữ kết quả truy vấn trong cache ⇒ giảm tải cho DNS server và cải thiện hiệu suất
      + DNS backup : cho biết số serial ⇒ sao lưu , update zone data
    - ***ns1.vmware.lab. / ns2.vmware.lab.*** : tên của primary server
    - ***admin.vmware.lab.*** : địa chỉ email cho domain name admin
    - ***vn.vmware.lab*** : subdomain name
    - ***2  ; Serial*** : số phiên bản (zones)
    - ***604800  ; Refresh*** : thời gian làm mới - khoảng tgian mà DNS server cập nhật bản ghi mới từ primary server
    - ***86400  ; Retry*** : thời gian thử lại - khoảng tgian mà DNS server sẽ chờ trước khi thử lại một yêu cầu DNS tới server khác
    - ***2419200  ; Expire*** : thời gian hết hạn - khoảng tgian mà DNS server lưu trữ bản ghi đã nhận được từ primary server trước khi nó ko còn được sử dụng
    - ***604800   ; Minimum TTL*** : thời gian sống tối thiểu của các bản ghi trong zone
  => mỗi lần edit 1 file zone → cần tăng Serial lên 1 trước khi restart lại bind9
  + Check configure syntax
  ```
    $ sudo named-checkconf
    $ sudo named-checkzone vmware.lab /etc/bind/zones/db.vmware.lab
    $ sudo named-checkzone 188.168.192.in-addr.arpa /etc/bind/zones/db.192.168.188
  ```
  + Restart and allow bind9
  ```
    $ sudo service bind9 restart
    $ sudo ufw allow Bind9
  ```
##### c. Configure DNS Server Slave
- Cài đặt chương trình bind9
  ```
    $ sudo apt update
    $ sudo apt install bind9 bind9utils bind9-doc
  ```
- Start bind9 DNS server
  ```
    $ sudo service bind9 start 
    $ sudo service bind9 status
  ```
- Cấu hình các thông số cho bind9 DNS Slave 1
  + Cấu hình file "/etc/default/named"
    ```
      $ sudo vim /etc/default/named
    ```
    ```bash
      OPTIONS="-u bind -4"
    ```
  + Cấu hình file "/etc/named.conf.options"
  ```
    $ sudo vim /etc/bind/named.conf.options
  ```
  ```bash
    acl "trusted" {
        192.168.188.131;    # ns1 
        192.168.188.132;   # ns2
        192.168.188.130;  # host1
    };

    logging {
      channel bind_log {
        file "bind.log" versions 3 size 250k;
        severity info;
      };
      category default {
        bind_log;
      };
    };

    options {
      directory "/var/cache/bind";

      dnssec-validation auto; #auto-verification integrated with DNSSEC
      recursion yes;  # enables recursive queries
      allow-recursion { trusted; }; # allows recursive queries from "trusted" clients
      listen-on { 192.168.188.132; }; # ns1 private IP address - listen on private network only
      allow-transfer { none; }; # disable zone transfers by default
      forwarders {
              8.8.8.8;
              8.8.4.4;
      };
    };

  ```
  + Cấu hình file "/etc/named.conf.local"
  ```
    $ sudo vim /etc/bind/named.conf.local
  ```
  ```bash
    # Định nghĩa Forward zone "vmware.lab" 
    zone "vmware.lab" {
      type slave;
      file "db.vmware.lab"; #zone file path
      masters{192.168.188.130;}; #master
    };

    # Định nghĩa Reverse zone "188.168.192.in-addr.arpa" ứng với Forward zone
    zone "188.168.192.in-addr.arpa" {
      type slave;
      file "db.192.168.188"; #192.168.188.0/24 subnet
      masters{192.168.188.130;}; #master
    };
  ```
    + Restart and allow bind9
  ```
    $ sudo named-checkconf
    $ sudo service bind9 restart
    $ sudo ufw allow Bind9
  ```
- Cấu hình các thông số cho bind9 DNS Slave 2
  + Cấu hình file "/etc/default/named"
    ```
      $ sudo vim /etc/default/named
    ```
    ```bash
      OPTIONS="-u bind -4"
    ```
  + Cấu hình file "/etc/named.conf.options"
  ```
    $ sudo vim /etc/bind/named.conf.options
  ```
  ```bash
    acl "trusted" {
        192.168.188.131;    # ns1 
        192.168.188.132;   # ns2
        192.168.188.130;  # host1
    };

    logging {
      channel bind_log {
        file "bind.log" versions 3 size 250k;
        severity info;
      };
      category default {
        bind_log;
      };
    };

    options {
      directory "/var/cache/bind";

      dnssec-validation auto; #auto-verification integrated with DNSSEC
      recursion yes;  # enables recursive queries
      allow-recursion { trusted; }; # allows recursive queries from "trusted" clients
      listen-on { 192.168.188.132; }; # ns1 private IP address - listen on private network only
      allow-transfer { none; }; # disable zone transfers by default
      forwarders {
              8.8.8.8;
              8.8.4.4;
      };
    };

  ```
  + Cấu hình file "/etc/named.conf.local"
  ```
    $ sudo vim /etc/bind/named.conf.local
  ```
  ```bash
    # Định nghĩa Forward zone "vmware.lab" 
    zone "vmware.lab" {
      type slave;
      file "db.vmware.lab"; #zone file path
      masters{192.168.188.130;}; #master
    };

    # Định nghĩa Reverse zone "188.168.192.in-addr.arpa" ứng với Forward zone
    zone "188.168.192.in-addr.arpa" {
      type slave;
      file "db.192.168.188"; #192.168.188.0/24 subnet
      masters{192.168.188.130;}; #master
    };
  ```
  + Restart and allow bind9
  ```
    $ sudo named-checkconf
    $ sudo service bind9 restart
    $ sudo ufw allow Bind9
  ```
##### d. Configure DNS Clients ( DNS authoritative)
- check network interface
  ```
   $ ip a
  ```
  => network interface : ens33 và dải mạng 192.168.188.0/24
- config new network 
  ```
    $ sudo vim /etc/netplan/00-private-nameservers.yaml
  ```
  ```yaml
    network:

    version: 2

    ethernets:

        ens33:          # Private network interface

            nameservers:

                addresses:

                - 192.168.188.131         # Private IP for ns1

                - 192.168.188.132         # Private IP for ns2

                - 192.168.188.134         # Private IP for ns3

                search: [ vmware.lab ]    # DNS zone
  ```
- confirm new network 
  ```
    $ sudo netplan try
  ```
##### e. Test DNS
- update file resolv.conf
  ```
    $ sudo ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
    $ cat /etc/resolv.conf
  ```
- lookup DNS 
  ```
    $ nslookup 192.168.188.130
    $ nslookup vn.vmware.lab
  ```
- Check the operation of DNS Server Master - Slave 
  + check status of 2 Server
    ```
      $ sudo service bind9 status
    ```
  + stop bind9 of server Master
    ```
    $ sudo service bind9 stop
    $ sudo service bind9 status
    ```
  + lookup IP address/domain name in DNS client
    ```
      $ nslookup 192.168.188.130
      $ nslookup vn.vmware.lab
    ```
  + client query 1 domain in zone but has not record
    ```
    $ nslookup hanoi.vmware.lab
    ```
    ![Alt text](images/image2.png)
    => DNS Server sẽ phản hồi về cho Client  : "NXDOMAIN" ( domain no exits )
  + client query 1 domain không thuộc zone
    ```
      $ nslookup dantri.com.vn
    ```
    ![Alt text](images/image4.png)
  => DNS Server sẽ check config zones trong nó => không thấy => thực hiện quá trình phân giải đệ quy =>  truy vấn đến Root DNS Server => truy vấn đến TLD DNS Server => truy vấn đến authoritative DNS Server của domain đó ( dantri.com.vn ) => phản hồi về cho DNS Resolver => phản hồi về cho Client 
## Monitoring DNS Server 
##### Command 
- ping 
  + kiểm tra tính khả dụng của máy chủ DNS
  + command : **ping <your_dns_server_name/ip>**
- traceroute
  + kiểm tra đường đi và độ trễ khi truy vấn DNS server
  + command : **traceroute <your_dns_server_name/ip>**
- dig
  + truy vấn DNS và xem kết quả chi tiết về các bản records
  + command : **dig -type=<record_name> <name_server>**
- nslookup
  + truy vấn DNS và xem kết quả về name và IP address
  + command : **nslookup <name_server/IP_address>**
- nmap
  + xem trạng thái các kết nối mạng và cổng mạng đang hoạt động
  + command : **nmap -p 53 <name_server>**
- view file log
  + kiểm tra các sự kiện và thông tin quan trọng
  + command : **tail -f /var/cache/bind/bind.log**

